package fr.unix_experience.owncloud_sms.defines;

public class DefaultPrefs {
	public final static Integer syncInterval = 15;
    public final static Integer minimumCharsForSync = 0;
	public final static Boolean pushOnReceive = true;
	public final static Boolean showSyncNotifications = true;
	public final static Boolean syncOnlyOnWiFi = true;
}
